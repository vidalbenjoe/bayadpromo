//
//  PromoViewController.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import UIKit

protocol PromoDisplayLogic: class {
    func displayPromos(viewModel: PromoEntity.ViewModel)
    func displayError(message: String)
    func reloadView()
}

class PromoViewController: BaseViewController, PromoDisplayLogic {
   
    @IBOutlet weak var tableview: UITableView!
    var promos: [Promos] = []
    var interactor: PromoBusinessLogic?
    var router: (NSObjectProtocol & PromoRoutingLogic & PromoRoutingDataPassing)?
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        ClassBuilder.promoModule(arroundView: self)
    }
    
    // MARK: Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(PromoViewController.longPress(longPressGestureRecognizer:)))
        self.view.addGestureRecognizer(longPressRecognizer)
    }
    
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.tableview)
            if let indexPath = tableview.indexPathForRow(at: touchPoint) {
                let promo = self.promos[indexPath.row]
                if let id = promo.id{
                    showDeleteAlert(promoID: id)
                }
            }
        }
    }
    
    func setupTableView(){
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "PromoTableViewCell", bundle: nil), forCellReuseIdentifier: "PromoTableViewCell")
        tableview.rowHeight = 100
        tableview.estimatedRowHeight = 100
        tableview.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.fetchPromos()
    }
    
    func reloadData(){
        tableview.reloadData()
    }
    
    func displayPromos(viewModel: PromoEntity.ViewModel) {
        if let promo = viewModel.promos{
            self.promos = promo
            reloadData()
        }
    }
    
    func displayError(message: String) {
       print(message)
    }
    
    func reloadView() {
        reloadData()
    }
    
    func showDeleteAlert(promoID: String) {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this item?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
        
        }))
        alert.addAction(UIAlertAction(title: "Delete",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        self.interactor?.deletePromo(promoID: promoID)
                                      }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension PromoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.promos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromoTableViewCell", for: indexPath) as? PromoTableViewCell
        let promo = self.promos[indexPath.row]
        cell?.apply(promo: promo)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        router?.routeToPromoDetail(segue: nil) // Route to Promo Details via PromoRouter
    }
}
