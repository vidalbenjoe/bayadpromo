//
//  PromoTableViewCell.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import UIKit
import SnapKit
class PromoTableViewCell: UITableViewCell {
    @IBOutlet weak var promoNameLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    
    @IBOutlet weak var readIndicator: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
      super.layoutSubviews()
        readIndicator.layer.cornerRadius = readIndicator.frame.size.width/2
    }
    
    func apply(promo: Promos){
        if let name = promo.name, let details = promo.details, let isRead = promo.read {
            promoNameLbl.text = name
            detailsLbl.text = details
            
            if isRead == 1 {
                readIndicator.isHidden = false
                self.contentView.backgroundColor = UIColor(hexString: "#E9EFF5")
            }else{
                readIndicator.isHidden = true
                self.contentView.backgroundColor = UIColor(hexString: "#FFFFFF")
                promoNameLbl.snp.makeConstraints { (make) -> Void in
                         make.left.equalTo(16)
                }
            }
        }
    }
    
}
