//
//  PromoInteractor.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation


protocol PromoBusinessLogic {
    func fetchPromos()
    func deletePromo(promoID: String)
}

protocol PromoDataStore {
    var promo: PromoEntity.Response! {get set}
}

class PromoInteractor: PromoBusinessLogic, PromoDataStore {
    var promo: PromoEntity.Response!
    private let worker: APIWorkerProtocol
    var presenter: PromoPresentationLogic?
    
    required init(withApiWorker apiWorker: APIWorkerProtocol) {
        self.worker = apiWorker
    }
    
    func fetchPromos() {
        worker.fetchPromos() { (result) in
        switch(result){
        case .success(let data):
            self.promo = data
            self.presenter?.interactor(self, didFetch: data)
            break;
        case .failure(let error):
            self.presenter?.interactor(self, didFailWith: error)
            break
            }
        }
    }
   
    func deletePromo(promoID: String) {
        worker.deletePromo(promoID: promoID) { (result) in
        switch(result){
        case .success(let data):
            self.presenter?.interactor(self, didFetch: data)
            break;
        case .failure(let error):
            print("mmfmwf", error.localizedDescription)
            self.presenter?.interactor(self, didFailWith: error)
            break
            }
        }
    }
    
}
