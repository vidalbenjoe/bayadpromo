//
//  PromoRouter.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import UIKit

@objc protocol PromoRoutingLogic{
    func routeToPromoDetail(segue: UIStoryboardSegue?)
}

protocol PromoRoutingDataPassing
{
  var dataStore: PromoDataStore? { get } // Reference from interactor - This DataStore will be responsible for storing data received from interactor.
}

class PromoRouter: NSObject, PromoRoutingLogic, PromoRoutingDataPassing{

    weak var viewController: PromoViewController?
    var dataStore: PromoDataStore?
    
    func routeToPromoDetail(segue: UIStoryboardSegue?) {
        if let segue = segue {
            let destinationVC = segue.destination as! PromoDetailsViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToPromoDetails(source: dataStore!, destination: &destinationDS)
        } else {
            let destinationVC = viewController?.storyboard?.instantiateViewController(withIdentifier: "PromoDetailsViewController") as! PromoDetailsViewController
            
            var destinationDS = destinationVC.router!.dataStore!
            passDataToPromoDetails(source: dataStore!, destination: &destinationDS)
            navigateToPromoDetails(source: viewController!, destination: destinationVC)
        }
    }
    
    
    // MARK: Navigation
    func navigateToPromoDetails(source: PromoViewController, destination: PromoDetailsViewController){
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    func passDataToPromoDetails(source: PromoDataStore, destination: inout PromoDetailsDataStore){
        let selectedRow = viewController?.tableview.indexPathForSelectedRow?.row
        if let promos = source.promo.promos {
            let selectedOrder = promos[selectedRow!]
            destination.promo = selectedOrder
        }
    }
}

