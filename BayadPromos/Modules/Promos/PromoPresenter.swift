//
//  PromoPresenter.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation


protocol PromoPresentationLogic {
    /// The Interactor will inform the Presenter a successful fetch.
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: PromoEntity.Response)
    
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: String)
    /// The Interactor will inform the Presenter a failed fetch.
    func interactor(_ interactor: PromoBusinessLogic, didFailWith error: Error)
    
     
}

class PromoPresenter: NSObject, PromoPresentationLogic {
    
    weak var viewController: PromoViewController?
    var interactor: PromoBusinessLogic?
    
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: PromoEntity.Response) {
        let viewModel = PromoEntity.ViewModel(promos: object.promos)
        viewController?.displayPromos(viewModel: viewModel)
    }
    
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: String) {
        viewController?.reloadView()
    }
    
    func interactor(_ interactor: PromoBusinessLogic, didFailWith error: Error) {
        viewController?.displayError(message: error.localizedDescription)
    }
    
}

