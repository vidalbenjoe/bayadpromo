//
//  PromoDetailsPresenter.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation


protocol PromoDetailsPresentationLogic
{
    func presentPromo(promo: Promos)
    
    /// The Interactor will inform the Presenter a successful fetch.
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: PromoEntity.Response)
    /// The Interactor will inform the Presenter a failed fetch.
    func interactor(_ interactor: PromoBusinessLogic, didFailWith error: Error)
}

class PromoDetailsPresenter: PromoDetailsPresentationLogic
{
  weak var viewController: PromoDetailsViewController?
  
  
  func presentPromo(promo: Promos)
  {
    viewController?.displayPromoDetails(viewModel: promo)
  }
    
    func interactor(_ interactor: PromoBusinessLogic, didFetch object: PromoEntity.Response) {
        
    }
    
    func interactor(_ interactor: PromoBusinessLogic, didFailWith error: Error) {
        
    }
}
