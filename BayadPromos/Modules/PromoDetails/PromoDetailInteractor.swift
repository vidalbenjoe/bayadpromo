//
//  PromoDetailInteractor.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation

protocol PromoDetailsBusinessLogic{
    func fetchPromoDetails()
    func updatePromo(promoID: String, request: PromoEntity.Request)
}

protocol PromoDetailsDataStore
{
    var promo: Promos! { get set }
}

class PromoDetailInteractor: PromoDetailsBusinessLogic, PromoDetailsDataStore
{

    var promo: Promos!
    var presenter: PromoDetailsPresentationLogic?
    private let worker: APIWorkerProtocol
    
    required init(withApiWorker apiWorker: APIWorkerProtocol) {
        self.worker = apiWorker
    }
    
    
    func fetchPromoDetails() {
        self.presenter?.presentPromo(promo: promo)
    }
    
    func updatePromo(promoID: String, request: PromoEntity.Request) {
        worker.updatePromo(promoID: promoID, request: request) { (result) in
            switch(result){
            case .success(let data):
                break;
            case .failure(let error):
                print("leerrror", error.localizedDescription)
                break
            }
        }
    }
    
}
