//
//  PromoDetailsViewController.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import UIKit
import SDWebImage

protocol PromoDetailsDisplayLogic: class {
    func displayPromoDetails(viewModel: Promos)
}

class PromoDetailsViewController: BaseViewController, PromoDetailsDisplayLogic {
   
    var interactor: PromoDetailsBusinessLogic?
    var router: (NSObjectProtocol & PromoDetailsRoutingLogic & PromoDetailsDataPassing)?
    
    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var promoNameLbl: UILabel!
    @IBOutlet weak var promoDescriptionTextView: UITextView!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        ClassBuilder.promoDetailsModule(arroundView: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.fetchPromoDetails()
        self.title = "Promos"
    }
    
    func displayPromoDetails(viewModel: Promos) {
        if let id = viewModel.id, let name = viewModel.name, let description = viewModel.details, let imageURL = viewModel.imageURL, let read = viewModel.read {
            promoNameLbl.text = name
            promoDescriptionTextView.text = description
            promoImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: imageURL))
            if read == 1{
                let request = PromoEntity.Request(name: name, details: description, image_url: imageURL, read: 0)
                interactor?.updatePromo(promoID: id, request: request)
            }
        }
    }
}
