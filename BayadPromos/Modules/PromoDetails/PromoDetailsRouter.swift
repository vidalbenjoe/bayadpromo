//
//  PromoDetailsRouter.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation
@objc protocol PromoDetailsRoutingLogic
{
  //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol PromoDetailsDataPassing
{
  var dataStore: PromoDetailsDataStore? { get }
}

class PromoDetailsRouter: NSObject, PromoDetailsRoutingLogic, PromoDetailsDataPassing {
    weak var viewController: PromoDetailsViewController?
    var dataStore: PromoDetailsDataStore?
    
}
