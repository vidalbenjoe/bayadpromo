//
//  ClassBuilder.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation
class ClassBuilder: NSObject {
    
    class func promoModule(arroundView viewController: PromoViewController){
        let presenter = PromoPresenter()
        let interactor = PromoInteractor(withApiWorker: APIWorker())
        let router = PromoRouter()
        
        //MARK: link VIP components.
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        
        router.viewController = viewController
        router.dataStore = interactor
        
    }
    
    class func promoDetailsModule(arroundView viewController: PromoDetailsViewController){
        let presenter = PromoDetailsPresenter()
        let interactor = PromoDetailInteractor(withApiWorker: APIWorker())
        let router = PromoDetailsRouter()
        
        //MARK: link VIP components.
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        
        router.viewController = viewController
        router.dataStore = interactor
        
    }
    
}
