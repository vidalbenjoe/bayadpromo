//
//  PromoEntity.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation


enum PromoEntity {
    
    struct Request: Codable{
        let name, details: String?
        let image_url: String?
        let read: Int?
    }
    
    struct Response: Codable {
        var promos: [Promos]?
    }
    
    struct ViewModel {
        var promos: [Promos]?
    }
    
}
