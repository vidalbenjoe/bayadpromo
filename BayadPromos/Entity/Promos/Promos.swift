//
//  Promos.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation


struct Promos: Codable {
    let id, name, details: String?
    let imageURL: String?
    let read: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, details
        case imageURL = "image_url"
        case read
    }
}
