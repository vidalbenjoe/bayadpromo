//
//  APIClient.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation
import Alamofire

struct APIClient {
    static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>) -> Void) {
        AF.request(route).responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
            completion(response.result)
        }
    }
}
