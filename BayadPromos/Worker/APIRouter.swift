//
//  APIRouter.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation
import Alamofire
enum APIRouter: URLRequestConvertible {
    case fetchpromos
    case updatepromo(promoID: String, request: Parameters)
    case deletepromo(promoID: String)

    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .fetchpromos:
            return .get
        case .updatepromo:
            return .put
        case .deletepromo:
            return .delete
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .fetchpromos:
            return Constants.Routes.fetchPromoEndpoint
        case .updatepromo(let promoID, _):
            return Constants.Routes.updatePromoEndpoint + promoID
        case .deletepromo(let promoID):
            return Constants.Routes.deletePromoEndpoint + promoID
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .fetchpromos:
            return nil
        case .deletepromo:
            return nil
        case .updatepromo(_, let request):
            return request
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.Endpoints.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
       
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
     
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
    
}
