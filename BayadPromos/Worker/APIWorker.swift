//
//  APIWorker.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation
import Alamofire

protocol APIWorkerProtocol {
    func fetchPromos(callback: @escaping(Result<PromoEntity.Response, Error>) -> Void)
    func updatePromo(promoID: String, request: PromoEntity.Request, callback: @escaping (Result<PromoEntity.Response, Error>) -> Void)
    func deletePromo(promoID: String, callback: @escaping (Result<String, Error>) -> Void)
}

class APIWorker: APIWorkerProtocol {
    // MARK: FETCH PROMOS
    func fetchPromos(callback: @escaping (Result<PromoEntity.Response, Error>) -> Void) {
        let route = APIRouter.fetchpromos
        APIClient.performRequest(route: route) { (result: Result<[Promos], AFError>) in
            switch(result){
            case .success(let data):
                var promos = PromoEntity.Response()
                promos.promos = data
                callback(.success(promos))
                break
            case .failure(let error):
                callback(.failure(error))
                break
            }
        }
    }
    // MARK: UPDATE PROMO
    func updatePromo(promoID: String, request: PromoEntity.Request, callback: @escaping (Result<PromoEntity.Response, Error>) -> Void) {
    
        do{
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(request)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            
            if let jsonParam = json {
                let dict = Utilities().convertToDictionary(text: jsonParam)
                guard let dataDictionary = dict else{
                    return
                }
                
                let route = APIRouter.updatepromo(promoID: promoID, request: dataDictionary)
                APIClient.performRequest(route: route) { (result: Result<[Promos], AFError>) in
                    switch(result){
                    case .success(let data):
                        var promos = PromoEntity.Response()
                        promos.promos = data
                        callback(.success(promos))
                        break
                    case .failure(let error):
                        callback(.failure(error))
                        break
                    }
                }
                
            }
        } catch {
            print(error)
        }
    }
    
    // MARK: DELETE PROMO
    func deletePromo(promoID: String, callback: @escaping (Result<String, Error>) -> Void) {
        let route = APIRouter.deletepromo(promoID: promoID)
        APIClient.performRequest(route: route) { (result: Result<String, AFError>) in
            switch(result){
            case .success(let data):
                callback(.success(data))
                break
            case .failure(let error):
                callback(.failure(error))
                break
            }
        }
    }
    
}
