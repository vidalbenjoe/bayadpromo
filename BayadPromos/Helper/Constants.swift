//
//  Constants.swift
//  BayadPromos
//
//  Created by Benjoe Vidal on 3/13/21.
//

import Foundation

struct Constants{
    struct Endpoints {
        static let baseURL = "https://crudcrud.com/api/a911c7a3e38841c88bac4eeca1252343"
    }
        
    struct Routes{
        static let fetchPromoEndpoint = "/bayad"
        static let updatePromoEndpoint = "/bayad/"
        static let deletePromoEndpoint = "/bayad/"
    }
}

enum HTTPHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }

 enum ContentType: String {
        case json = "application/json"
        case text = "text/html"
 }
